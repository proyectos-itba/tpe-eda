# TPE-EDA

Reversi minimax

- sources root = src/main/java/tpe

## Requerimientos

- maven
- jdk 1.8+

## Para compilar

```bash

    mvn clean install

```

El archivo .jar generado se creara en la carpeta target (correr ejecutando java -jar target/tpe-1.0.jar)

## Metricas

Las metricas del informe fueron calculadas utilizando el archivo 'metricas.m', que es un tablero en el turno 5.

