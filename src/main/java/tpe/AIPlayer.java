package tpe;

import java.awt.*;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
public class AIPlayer implements  Player, Serializable {
    int playerNumber;
    int optionId = 0;
    StringBuffer lastMoveTree;
    long startTime;
    long maxTime;
    Map<String,Integer> options;
    Player[] players;
    int maxDepth;

    AIPlayer(int playerNumber, Map<String,Integer> options, Player[] players) {
        this.playerNumber = playerNumber;
        this.options = options;
        this.players = players;
    }

    @Override
    public boolean isAi() {
        return true;
    }

    public int[] getMove(Game.Board board) {

        // Wrapper para el nivel 1 del minimax, que maneja los multiples modos

        // La id se usa para la construccion del arbol en formato DOT
        optionId = 0;
        int pn;
        if (players[0] == this) {
            pn = 0;
        } else {
            pn = 1;
        }

        // Modo 1 - por tiempo
        this.startTime = System.currentTimeMillis();
        if (options.get("mode") == 1) {
            maxTime = options.get("param") * 1000;
        }


        List<Game.Board> adj = board.getValidMoves(pn);

        // Cargo el primer valor como opcion default
        Node bestMove = new Node(adj.get(0));
        bestMove.score = Integer.MIN_VALUE;

        // Creo el buffer para el arbol Dot
        StringBuffer str = new StringBuffer();
        str.append("graph tree {\n");
        Node firstNode;

        // Modo por tiempo ( Mientras haya tiempo, probamos hacer hasta nivel 3, y si sobra tiempo vamos
        // aumentando de a 2 niveles. En el momento que se acaba el tiempo, vuelve arriba de todo y devuelve
        // El movimiento de la vuelta anterior ( que si llego a ser completa)
        if (options.get("mode") == 1) {
            int maxLevel = 3;
            while(System.currentTimeMillis() - startTime < maxTime) {
                optionId = 0; // Reseteo las Ids para saber cuantos nodos totales hubo
                firstNode = new Node(board);
                firstNode.id = optionId;
                Node localBestMove = new Node(adj.get(0));
                localBestMove.score = Integer.MIN_VALUE;
                StringBuffer localStr = new StringBuffer();
                localStr.append("graph tree {\n");
                for(Game.Board b : adj){
                    Node n = new Node(b);
                    n = minimax(n, false, 1, maxLevel,startTime, str, firstNode, localBestMove);

                    if (n == null) {
                        // Me quede sin tiempo, tengo q salir de los proximos checkeos
                        break;
                    }
                    if (n.score > localBestMove.score) {
                        localBestMove = n;
                    }

                }
                if (System.currentTimeMillis() - startTime < maxTime) {
                    // Si todavia me queda tiempo, significa que llego hasta abajo
//                    System.out.println("Succesfully finished level "+ maxLevel);
                    bestMove = localBestMove;
                    maxLevel += 2;
                    str = localStr;
                } else {
//                    System.out.println("Unable to reach level " + maxLevel);
                }
            }
        } else {
            firstNode = new Node(board);
            firstNode.id = optionId;
            for(Game.Board b : adj){
                Node n = new Node(b);
                n = minimax(n, false, 1, options.get("param"),startTime, str, firstNode, bestMove);
                if (n.score > bestMove.score) {
                    bestMove = n;
                }

            }
        }
        str.append(bestMove.id + " [style=filled,color=red, fontcolor=white];\n");
        str.append(0+" [shape=box][label=\"" + bestMove.score + "\"]" + ";\n");
        str.append('}');

        // Paso el DOT al clipboard ( solo para development)
//        Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();
//        StringSelection selection = new StringSelection(str.toString());
//        c.setContents(selection, selection);

        // Debug ( imprimo el tiempo que tardo en computar el movimiento)
//         System.out.println("Elapsed time: " + (System.currentTimeMillis() - startTime ) + "ms");
        this.lastMoveTree = str;
        return bestMove.board.lastMove;
    }


    private Node minimax(Node node, boolean max,int level, int maxLevel, long startTime, StringBuffer str, Node parent, Node parentBestMove){
        node.id = ++optionId;

        Node bestMove;
        List<Game.Board> adj;
        if(maxLevel <= level){
            if (level > maxDepth) {
                //System.out.println("nuevo max level: " + maxDepth);
                maxDepth = maxLevel;
            }
//           Devolvemos el score
            int heuristic = node.board.heuristic();
            str.append(parent.id + " -- " + node.id + ";\n");
            if (node.board.lastMove != null) {
                str.append(node.id+ " [shape=diamond][label=\"[" + node.board.lastMove[0] + ", " + node.board.lastMove[1] + "] " + heuristic * playerNumber + "\"]");
            } else {
                str.append(node.id+ " [shape=diamond][label=\"(pass) " + heuristic * playerNumber + "\"]");
            }
            str.append(";\n");
            node.score = node.board.heuristic() * playerNumber;
            return node;
        } else if(options.get("mode") == 1 && maxTime < (System.currentTimeMillis() - startTime)){
            // Me quede sin tiempo, devuelvo null y uso el mejor q haya quedado del nviel anterior
            return null;
        }

        adj = node.board.getValidMoves(node.board.playerNumber);
        bestMove = new Node(adj.get(0));
        if (max) {
            bestMove.score = Integer.MIN_VALUE;
        } else {
            bestMove.score = Integer.MAX_VALUE;
        }
        boolean pruneRest = false;

        for(Game.Board b : adj){
            if (pruneRest && options.get("prune") == 1) {
                // Prunning
                Node n = new Node(b);
                n.id = ++optionId;
                str.append(n.id + " [label=\"[" + n.board.lastMove[0] + ", " + n.board.lastMove[1] + "]" + "\"][style=filled,color=gray, fontcolor=white];");
                if (!max) {
                    str.append("[shape=box]");
                }
                str.append("\n");
                str.append(node.id + " -- " + n.id + "\n");
            } else {

                Node n = new Node(b);
                n = minimax(n, !max, level+1, maxLevel, startTime, str, node, bestMove);
                if ( n == null) {
                    return null;
                }
                if (max) {
                    if (n.score > bestMove.score) {
                        bestMove = n;
                    }
                } else {
                    if (n.score < bestMove.score) {
                        bestMove = n;
                    }
                }

                // Chekeo si hay q podar
                // Si yo soy min, mi padre busca el maximo. supongamos que su maximo local es 4
                // Si yo detecto algun valor menor(o igual) a cuatro, mi minimo si o si va a ser menor que 4, y no voy a ser elegido
                if (max) {
                    if (parentBestMove.id != node.id && bestMove.score >= parentBestMove.score) {
                        pruneRest = true;
                    }
                } else {
                    if (parentBestMove.id != node.id && bestMove.score <= parentBestMove.score) {
                        pruneRest = true;
                    }
                }
            }
        }
        node.score = bestMove.score;

        // Label
        if (node.board.lastMove != null) {
            str.append(node.id+" [label=\"[" + node.board.lastMove[0] + ", " + node.board.lastMove[1] + "] " + node.score + "\"]");
        } else {
            str.append(node.id+" [label=\"(pass) " + node.score + "\"]");
        }
        if (max) {
            str.append("[shape=box]");
        }
        str.append(";\n");

        // Connection
        str.append(parent.id + " -- " + node.id + ";\n");

        // Pinto el hijo elegido
        str.append(bestMove.id + " [style=filled,color=red, fontcolor=white]\n");
        return node;

    }



    @Override
    public int getNumber() {
        return playerNumber;
    }

    @Override
    public String label() {
        return "AI" + (playerNumber == 1? "(White)" : "(Black)");
    }

    private class Node{
        int score;
        Game.Board board;
        int id;

        public Node(Game.Board board) {
            this.board = board;
        }
    }
}
