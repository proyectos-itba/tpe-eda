package tpe;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.Deque;

public class FileManager {

    private static FileManager ourInstance = new FileManager();

    public static FileManager getInstance() {
        return ourInstance;
    }

    private FileManager() {}

    public void save(Component parent, Object toSaveObject) {
        try {

            JFileChooser fileChooser = new JFileChooser();
            if (fileChooser.showSaveDialog(parent) == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();
                FileOutputStream fileOut = new FileOutputStream(file);
                ObjectOutputStream out = new ObjectOutputStream(fileOut);
                out.writeObject(toSaveObject);
                out.close();
                fileOut.close();
            }
        } catch (IOException i) {
            JOptionPane.showMessageDialog(null, "Error. Unable to save file.");
            System.out.println("Unable to save file. There was a problem saving the file.");
            System.out.println(i);
        }
    }

    public void export(Component parent, String toSaveString) {
        try {

            JFileChooser fileChooser = new JFileChooser();
            if (fileChooser.showSaveDialog(parent) == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();
                FileOutputStream fileOut = new FileOutputStream(file);
                fileOut.write(toSaveString.getBytes());
                fileOut.close();
            }
        } catch (IOException i) {
                JOptionPane.showMessageDialog(null, "Error. Unable to save file.");
            System.out.println("Unable to save file. There was a problem saving the file.");
            System.out.println(i);
        }
    }

    public Deque<Game.Board> open(Component parent) { // Open(Path path) es la original, solo lo dejo asi para testear
        JFileChooser fileChooser = new JFileChooser();
        if (fileChooser.showOpenDialog(parent) == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            Deque<Game.Board> stack = openFile(file.getPath());
            return stack;
        }
        return null;
    }

    public Deque<Game.Board> openFile(String path) {
        try {
            FileInputStream fileIn = new FileInputStream(path);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            Deque<Game.Board> stack = (Deque<Game.Board>) in.readObject();
            in.close();
            fileIn.close();
            return stack;
        } catch (IOException | ClassNotFoundException | ClassCastException c) {
            JOptionPane.showMessageDialog(null, "Error. Unable to open file.");
            System.out.println("Unable to open file");
            System.out.println(c);
            return null;
        }
    }
}
