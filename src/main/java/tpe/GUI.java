package tpe;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import java.util.List;

public class GUI implements Serializable {
//    UI ELEMENTS
    public JFrame frame;
    private JLabel p1Label, p2Label, currentTurnLabel;
    private JPanel board, buttons;
    private int tileSize = 50;
    private int circleSize = 40;
    private Game game;
    public JOptionPane pane;
//    GUI Variables

    private BoardSquare[][] boardSquares;


    public GUI(Game game) {
        this.game = game;
        init();

    }

    public void init() {

        initFrame();
        initBoard();
        addGUIElements();
        repaintBoard();
        frame.repaint();
        frame.revalidate();
    }

    private void initFrame() {
        frame = new JFrame("Reversi minimax");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setForeground(Color.WHITE);
        Dimension DimMax = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setSize(1300,1000);
//        frame.setMaximumSize(DimMax);
//        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.getContentPane().setLayout(null);
        frame.setVisible(true);

    }

    private void initBoard() {
        int size = game.options.get("size");
        boardSquares = new BoardSquare[size][size];
        int baseX = 200 - (tileSize *  (size - 4)) / 2;

        board = new JPanel();
        board.setBounds(baseX,200, 200,200);
        board.setSize((size+1)* tileSize,tileSize * (size+1));

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                BoardSquare square = new BoardSquare(i,j, 0);
                boardSquares[i][j] = square;
                board.add(square);
            }
        }
        board.setVisible(true);
        board.repaint();
        frame.add(board);
    }

    public void resetBoard() {
        frame.removeAll();
        frame.repaint();
        init();
    }

    public void repaintBoard() {
        currentTurnLabel.setText("Current player: " + game.players[game.board.playerNumber].label());
        int[] scores = game.board.scores();
        p1Label.setText(p1Text() + "(" + scores[0] + ")");
        p2Label.setText(p2Text() + "(" + scores[1] + ")");
        int size = game.options.get("size");
        List<Game.Board> vm = game.board.getValidMoves(game.board.playerNumber);
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                boardSquares[i][j].availableMove = false;
                boardSquares[i][j].player = game.board.getTile(i,j);
                if (game.board.getTile(i, j) == 0) {
                    for(Game.Board b : vm) {

                        if (b.lastMove != null && (b.lastMove[0] == i && b.lastMove[1] == j)) {
                            boardSquares[i][j].availableMove = true;
                        }
                    }
                }
                boardSquares[i][j].repaint();
            }
        }
    }



    public void addGUIElements() {
        initButtons();
        initPlayer1Label();
        initPlayer2Label();
        initCurrentTurnLabel();
    }

    public void initButtons() {
        buttons = new JPanel();
//        buttons.setBackground(Color.BLUE);
        int size = game.options.get("size");
        int x = 200 - (tileSize *  (size - 4)) / 2;
        x += (size+1)* tileSize;
        int y = 200;
        buttons.setBounds(x, y, 100, 500);

//        Undo Button
        JButton undoBtn = new JButton("Undo");
        undoBtn.addActionListener(e -> {
            game.clickedUndo();
        });

        buttons.add(undoBtn);
        buttons.add(Box.createHorizontalStrut(100));

//        Save Btn
        JButton saveBtn = new JButton("Save");
        saveBtn.addActionListener(e -> {
            game.clickedSave();
        });
        buttons.add(saveBtn);
        buttons.add(Box.createHorizontalStrut(100));
//        Export Tree Btn
        JButton exportBtn = new JButton("Export");
        exportBtn.addActionListener(e -> {
            game.clickedExport();
        });
        buttons.add(exportBtn);
        buttons.add(Box.createHorizontalStrut(100));
//        Restart button
        JButton restartBtn = new JButton("Restart");
        restartBtn.addActionListener(e -> {
            game.clickedRestart();
        });
        buttons.add(restartBtn);


        //        Restart button
        JButton openBtn = new JButton("Open");
        openBtn.addActionListener(e -> {
            game.clickedOpen();
        });
        buttons.add(openBtn);


        frame.add(buttons);
        buttons.setVisible(true);


    }

    public String p1Text() {
        String text;
        switch(game.options.get("ai")) {
            case 0:
                text = "Human 1";
                break;
            case 1:
                text = "AI";
                break;
            case 2:
                text = "Human";
                break;
            default:
                throw new IllegalArgumentException();

        }
        return text;
    }

    public String p2Text() {
        String text;
        switch(game.options.get("ai")) {
            case 0:
                text = "Human 2";
                break;
            case 1:
                text = "Human";
                break;
            case 2:
                text = "AI";
                break;
            default:
                throw new IllegalArgumentException();

        }
        return text;
    }
    public void initPlayer1Label() {

        p1Label= new JLabel(p1Text());
        p1Label.setForeground(Color.BLACK);
        p1Label.setBounds(100, 105, 104, 16);
        p1Label.setHorizontalAlignment(SwingConstants.LEFT);
        frame.getContentPane().add(p1Label);
    }

    public void initCurrentTurnLabel() {
        currentTurnLabel= new JLabel("");
        currentTurnLabel.setForeground(Color.BLACK);
        int size = game.options.get("size");
        int y = 500 + (size - 4) * tileSize;
        int x = 240;
        currentTurnLabel.setBounds(x, y, 300, 16);
        frame.getContentPane().add(currentTurnLabel);
    }

    public void initPlayer2Label() {

        p2Label= new JLabel(p2Text());
        p2Label.setForeground(Color.BLACK);
        p2Label.setHorizontalAlignment(SwingConstants.RIGHT);
        p2Label.setBounds(490, 100, 100, 16);

        frame.getContentPane().add(p2Label);
    }

    private class BoardSquare extends JComponent
    {
        private int x;
        private int y;
        private int player;
        boolean availableMove = false;

        public BoardSquare(int x, int y, int player)
        {
            this.x = x;
            this.y = y;
            player = player;
            this.setBorder(new LineBorder(Color.BLUE, 1));
            this.setPreferredSize(new Dimension(tileSize, tileSize));
            this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                game.clickedSquare(x,y);
                e.consume();
            }
        });
        }

        public void paintComponent(Graphics g)
        {
            Graphics2D g2 = (Graphics2D) g;
            Rectangle box = new Rectangle(0,0,tileSize,tileSize);
            g2.draw(box);
            if (availableMove) {
                g2.setPaint(Color.LIGHT_GRAY);
            } else {
                g2.setPaint(Color.GRAY);
            }
            g2.fill(box);
            if(player == -1)
            {
                g2.setColor(Color.black);
                g2.fillOval((tileSize - circleSize)/ 2, (tileSize - circleSize)/ 2,circleSize ,circleSize);
                g2.drawOval((tileSize - circleSize)/ 2, (tileSize - circleSize)/ 2, circleSize, circleSize);
            }
            else if(player == 1) {
                g2.setColor(Color.white);
                g2.fillOval((tileSize - circleSize) / 2, (tileSize - circleSize) / 2, circleSize, circleSize);
                g2.drawOval((tileSize - circleSize) / 2, (tileSize - circleSize) / 2, circleSize, circleSize);
            }
        }
    }
}
