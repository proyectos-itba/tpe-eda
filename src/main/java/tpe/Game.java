package tpe;

import javax.swing.*;
import java.io.Serializable;
import java.util.*;
import java.awt.*;
import java.util.List;

public class Game  implements  Serializable {

    transient GUI window;
    public Player[] players;
    public Map<String,Integer> options = new HashMap<>();
    public Board board;
    private int size;
    Deque<Board> previousBoards;
    List<Board> currAvailableMoves;
    public boolean debug = false;


    public static void main(String[] args) {
        Game game = new Game(args);
        game.init();

    }

    public Game(String[] args) {
        loadOptions(args);

        // Checkeo si es null porque puede ser que se haya cargado un partido desde la consola
        if (this.board == null) {
            initGameState();
        }
        initPlayers();
    }

    private void initPlayers() {
        int aiMode = options.get("ai");
        players = new Player[2];
        if (aiMode == 0) {
            players[0] = new HumanPlayer(-1);
            players[1] = new HumanPlayer(1);
        } else if (aiMode == 1) {
            players[0] = new AIPlayer(-1, options, players);
            players[1] = new HumanPlayer(1);
        } else if (aiMode == 2) {
            players[0] = new HumanPlayer(-1);
            players[1] = new AIPlayer(1, options, players);
        }
    }

    public void init() {
        Game g = this;

        g.window = new GUI(g);
        g.window.frame.setVisible(true);
        if (currentPlayer().isAi()) {
            makeAIMove();
        }
    }

    public Player currentPlayer() {
        return players[board.playerNumber];
    }

    public void clickedSquare(int x, int y) {
        if (!currentPlayer().isAi()) {
            Board testBoard = new Board(board.cloneTiles(board.tiles));
            if(testBoard.tryMove(x,y, players[board.playerNumber])) {
                move(x, y);
            }
        }
    }

    public void makeAIMove() {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                Long startTime = System.currentTimeMillis();
                int[] nextMove = players[board.playerNumber].getMove(board);
                Long endTime = System.currentTimeMillis();
                Long duration = endTime - startTime;
                if  (duration < 1500) {
                    try {
                    Thread.sleep(1500 - duration);
                    } catch(Exception e) {

                    }
                }
                if (nextMove == null) {
                    skipTurn();
                } else {
                    move(nextMove[0], nextMove[1]);
                }
            }
        });
    }

    public void gameOver() {
        int[] scores = board.scores();
        int score = scores[0] - scores[1];
        if (score == 0) {
            JOptionPane.showMessageDialog(null, "It's a tie! Nobody loses");

        } else{
            Player winner = score > 0? players[0] : players[1];
            JOptionPane.showMessageDialog(null, "Game Is Over - " +( winner.getNumber() == 1? "White" : "Black")+ "(" + (winner.isAi()? "AI" : "Human") + ") is the winner");
        }
    }
    public void skipTurn() {
        List<Board> vm = board.getValidMoves(board.playerNumber);
        if (vm.size() == 1 && vm.get(0).passed) {
            Board testBoard = new Board(board.cloneTiles(board.tiles));
            testBoard.playerNumber = (board.playerNumber + 1) % 2;
            previousBoards.push(board);
            board = testBoard;
            window.repaintBoard();
            if (currentPlayer().isAi()) {
                makeAIMove();
            }

        } else {
            throw new RuntimeException("tried to skip when moves where available");
        }
    }
    public void move(   int x, int y) {

        /////////////// Move
        Board testBoard = new Board(board.cloneTiles(board.tiles));
        testBoard.playerNumber = board.playerNumber;
        if (!testBoard.tryMove(x,y,players[board.playerNumber])) {
            JOptionPane.showMessageDialog(null, "Error, se intento realizar un movimiento invalido");
        }
        previousBoards.push(board);
        board = testBoard;
        window.repaintBoard();
        /////////////////////////////
        if (board.gameOver()) {
            gameOver();
            return;
        }
        window.repaintBoard();
        if (currentPlayer().isAi()) {
            makeAIMove();
        } else {
            List<Board> vm = board.getValidMoves(board.playerNumber);
            if (vm.size() == 1 && vm.get(0).passed) {
                skipTurn();
            }
        }
    }

    public void loadOptions(String[] args) {
        loadDefaultOptions();

        int argCount = 0;
        for (String s : args) {
            // Estoy en un parametro, me fijo el string que le sigue
            if (s.charAt(0) == '-') {
                if (args.length >= argCount + 2) {
                    String value = args[argCount + 1];
                    loadOption(s,value);
                } else {
                    throw new IllegalArgumentException("\nInvalid parameter " +s + "\n");
                }
            }
            argCount++;
        }


    }

    public void loadOption (String op, String val) {
        if (op.equals("-size")) {
            int[] validValues = {4,6,8,10};

            int value = Integer.valueOf(val);
            boolean valid = false;
            for(int v : validValues) {
                if (v == value) valid = true;
            }
            if (!valid) {
                throw new IllegalArgumentException("\nInvalid parameter -size\n");
            }
            options.put("size", value);
        } else if (op.equals("-ai")) {
            int[] validValues = {0,1,2};

            int value = Integer.valueOf(val);
            boolean valid = false;
            for(int v : validValues) {
                if (v == value) valid = true;
            }
            if (!valid) {
                throw new IllegalArgumentException("\nInvalid parameter -ai\n");
            }
            options.put("ai", value);
        } else if (op.equals("-mode")) {
            String[] validValues = {"time", "depth"};

            boolean valid = false;
            for(String v : validValues) {
                if (v.equals(val)) valid = true;
            }
            if (!valid) {
                throw new IllegalArgumentException("\nInvalid parameter -mode\n");
            }
            if (val.equals("time")) {
                options.put("mode", 1);
            } else {
                options.put("mode", 0);
            }
        } else if (op.equals("-param")) {
            int min = 1;
            int max = 25;

            int value = Integer.valueOf(val);

            if (value < min || value > max) {
                throw new IllegalArgumentException("\nInvalid parameter -param (valid values between " + min + " and "+max + ")\n");
            }
            options.put("param", value);
        } else if (op.equals("-prune")) {
            String[] validValues = {"on", "off"};

            boolean valid = false;
            for (String v : validValues) {
                if (v.equals(val)) valid = true;
            }
            if (!valid) {
                throw new IllegalArgumentException("\nInvalid parameter -prune\n");
            }
            if (val.equals("on")) {
                options.put("prune", 1);
            } else {
                options.put("prune", 0);
            }
        } else if (op.equals("-load")) {
            Deque<Board> loadedBoards = FileManager.getInstance().openFile(val);
            this.previousBoards = loadedBoards;
            this.board = previousBoards.pop();
            this.options.put("size", this.board.tiles.length);
            this.size = this.options.get("size");
        } else {
            throw new IllegalArgumentException("\nInvalid parameter " + op + "\n");
        }
    }

    public void loadDefaultOptions() {
        options.put("ai", 1);
        options.put("size", 8);
        options.put("mode", 1);
        options.put("param", 1);
        options.put("prune", 1);
    }

    public void clickedUndo() {
        if (!currentPlayer().isAi() || board.gameOver()) {
           undo();
        }
    }

    public void clickedSave() {
        if (!currentPlayer().isAi()) {
            previousBoards.push(board);
            FileManager.getInstance().save(window.frame, previousBoards);
            previousBoards.pop();
        }
    }

    public void clickedOpen() {
        if (!currentPlayer().isAi()) {
            Deque<Board> loadedFile = FileManager.getInstance().open(window.frame);

            if (loadedFile != null) {
                previousBoards = loadedFile;
                board = previousBoards.pop();
                this.options.put("size", this.board.tiles.length);
                this.size = this.options.get("size");
                window.resetBoard();
                if (currentPlayer().isAi()) {
                    makeAIMove();
                }

            }
        }
    }

    public void clickedRestart() {
        if (!currentPlayer().isAi() || board.gameOver()) {
            restart();
        }
    }

    public void clickedExport() {
        if (!currentPlayer().isAi()) {
            AIPlayer player = null;
            if (players[0].isAi()) {
                player = (AIPlayer) players[0];
            }
            if (players[1].isAi()) {
                player = (AIPlayer) players[1];
            }
            if (player != null) {
                if (player.lastMoveTree == null) {
                    JOptionPane.showMessageDialog(null, "Error. Unable to generate DOT file (no movement has been made).");
                } else {
                    FileManager.getInstance().export(window.frame, player.lastMoveTree.toString());
                }
            }
        }
    }

    public void initGameState() {
        this.size = options.get("size");
        int[][] initialState = new int[size][size];
        previousBoards = new LinkedList<>();
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (i == size / 2 && j == size / 2) {
                    initialState[i][j] = -1;
                } else if (i == size / 2 - 1 && j == size / 2 - 1) {
                    initialState[i][j] = -1;
                } else if(i == size / 2 - 1 && j == size / 2) {
                    initialState[i][j] = 1;
                } else if(i == size / 2 && j == size / 2 - 1) {
                    initialState[i][j] = 1;
                }
            }
        }
        this.board = new Board(initialState);
        this.board.playerNumber = 0;
    }

    public void restart() {
        initGameState();
        initPlayers();

        window.repaintBoard();
        if (currentPlayer().isAi()) {
            makeAIMove();
        }
    }


    public void undo() {
        while(!previousBoards.isEmpty() && (players[previousBoards.peek().playerNumber].isAi() || (previousBoards.peek().getValidMoves(previousBoards.peek().playerNumber).size() == 1) && previousBoards.peek().getValidMoves(previousBoards.peek().playerNumber).get(0).passed)) {
            previousBoards.pop();
        }
        if (!previousBoards.isEmpty()) {
            board = previousBoards.pop();
            window.repaintBoard();
        }
    }


    public class Board implements Serializable {
        public int[][] tiles;
        public int[] lastMove = null;
        boolean passed = false;
        public int maxHeuristic = 21;
        public int playerNumber;



        public Board(int[][] tiles) {
            this.tiles = tiles;
        }

        public int getTile(int i, int j) {
            return tiles[i][j];
        }

        public boolean tryMove(int x, int y, Player player) {
            int playerNumber = player.getNumber();

            if (tiles[x][y] != 0) return false;
            boolean hasFlipped = false;
//            Tengo que ir hacia las 8 direcciones y ver si en alguna hago algun flip
//            Puedo hacer un for de 0 a 8 o hacer un doble for de -1 a 1. La segunda parece mas elegante
            for (int i = -1; i <= 1; i++) {
                for (int j = -1; j <= 1; j++) {
//                    Estoy en una direccion, por ejemplo una diagonal ( asi agarro caso de X e Y)
//                    Supongamos i = 1 j = 1 ( voy hacia abajo a la derecha)
                    int moves = 1; // Empiezo en la primera y me voy moviendo hasta llegar o a una del mismo color
                                   // o a una vacia
                    boolean hasEaten = false;
                    // Ciclo por toda la fila, hasta llegar a la punta del tablero o hasta cortar ( mismo color o vacio)

                    while(  inBoard(x + (moves * i), y + (moves * j)) && tiles[x + moves * i][y + moves * j] == (-1 * playerNumber)) {
//                      // Si estoy adentro del for significa que estoy pasando por tiles del color opuesto
                        // Las tiles por las que estoy pasando son las que se flipearian cuando hago el movimiento
                        hasEaten = true;
                        moves++; // Me muevo a la siguiente posicion
                    }

                    // Sali del ciclo, o me tope con una vacia, o una del mismo color, o se me acabo el tablero
                    // El movimiento es valido si o solo si la tile final es del color del jugador

                    int finalX = x + moves * i;
                    int finalY = y + moves * j;


                    if (inBoard(finalX, finalY) && tiles[finalX][finalY] == playerNumber && hasEaten) {
                        // El movimiento fue valido, hago los flips
                        for (int k = 0; k <= moves; k++) {
                            tiles[x + k * i][y + k * j] = playerNumber;
                        }
                        hasFlipped = true;

                    }
                }
            }

            if (hasFlipped) {
                lastMove = new int[2];
                lastMove[0] = x;
                lastMove[1] = y;
            }
            this.playerNumber = (this.playerNumber + 1) % 2;
            return hasFlipped;
        }

        public boolean inBoard(int x, int y) {
            return ((x >= 0) && (y >= 0) && (x < size) && (y < size));
        }

        public List<Board> getValidMoves(int playerNumber) {
            List<Board> possibleMoves = new LinkedList<>();
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    boolean occupiedAdjacentSquare = false;
                    for (int k = -1; k <=1; k++) {
                        for (int l = -1; l <= 1; l++) {
                            if (k != 0 || l != 0){
                                if (inBoard(i+k,j+l) && (tiles[i+k][j+l] != 0)) {
                                    occupiedAdjacentSquare = true;
                                }
                            }
                        }
                    }
                    if (occupiedAdjacentSquare) {
                        Board testBoard = new Board(cloneTiles(tiles));
                        testBoard.playerNumber = playerNumber;
                        if (testBoard.tryMove(i,j,players[playerNumber])) {
                            possibleMoves.add(testBoard);
                        }
                    }
                }
            }

            if (possibleMoves.size() == 0) {
                Board testBoard = new Board(cloneTiles(tiles));
                testBoard.passed = true;
                testBoard.playerNumber = (testBoard.playerNumber + 1) % 2;
                possibleMoves.add(testBoard);
            }
            return possibleMoves;
        }

        public Player[] getPlayers() {
            return players;
        }

        public String toString() {
            StringBuffer b = new StringBuffer();
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    b.append(tiles[i][j] + " " );
                }
                b.append('\n');
            }
            return b.toString();
        }


        public int heuristic() {
            return complexHeuristic();
        }

        public int simpleHeuristic() {

            int heuristic = 0;

            int[] scores = scores();
            // scores[0] = Cantidad de fichas del jugador 0 (negro) - scores[1] = Cantidad de fichas del jugador 1 (blanco)
            int scoreDiff = scores[1] - scores[0];
            // scoreDiff es la diferencia de los puntos


            // Me fijo si termino
            if (scores[0] + scores[1] == size * size) {
//                Gano blancas
                if (scoreDiff > 0) {
                    return 21;
                }
//                Gano negras
                if (scoreDiff < 0) {
                    return -21;
                }
//                Empate
                return 0;
            }


//            Juego no terminado, asigno valor a las esquinas (4 puntos a cada una)
            int[] esquinas = new int[4];
            esquinas[0] = tiles[0][0];
            esquinas[1] = tiles[0][size-1];
            esquinas[2] = tiles[size-1][0];
            esquinas[3] = tiles[size-1][size-1];
            for (int i = 0; i < 4; i++) {
                heuristic += 3 * esquinas[i]; // Si la esquina esta tomada por el negro vale -1, asi que restaria 3
            }

//          Asigno valor por diferencia de puntaje (4 puntos maximo)
            if (scoreDiff > 5) {
                heuristic += 4;
            } else if(scoreDiff < -5) {
                heuristic -= 4;
            } else if (scoreDiff > 2) {
                heuristic += 2;
            } else if (scoreDiff < -2) {
                heuristic -= 2;
            } else if (scoreDiff> 0) {
                heuristic+= 1;
            } else if (scoreDiff < 0) {
                heuristic -= 1;
            }
            return heuristic;
        }

        public int complexHeuristic() {
            // La heuristica va a ir de -21 a 21
            // Heuristica positiva = tablero bueno para el jugador blanco
            // Heuristica negativa = tablero bueno para el jugador negro

            List<Board> vm0 = getValidMoves(0); // Todos los movimientos del jugador 0 (NEGRO)

            List<Board> vm1 = getValidMoves(1); // Todos los movimientos del jugador 1 (Blanco)

            int[] scores = scores();
            // scores[0] = Cantidad de fichas del jugador 0 (negro) - scores[1] = Cantidad de fichas del jugador 1 (blanco)
            int scoreDiff = scores[1] - scores[0];
            // scoreDiff es la diferencia de las fichas, si es positivo significa que el blanco tiene mas fichas
            int blackMoves = vm0.size();
            int whiteMoves = vm1.size();

            boolean whiteHasMoves = !(whiteMoves == 1 && vm1.get(0).passed); // Si el unico movimiento es pasar, no tiene movimientos
            boolean blackHasMoves = !(blackMoves == 1 && vm0.get(0).passed); // Si el unico movimiento es pasar, no tiene movimientos

            // Primero deeterminamos si hubo algun ganador
            if (!whiteHasMoves && !blackHasMoves) { // Game over - Ningun jugador tiene movimientos
                if (scoreDiff > 0) {
                    return 21;
                } else if (scoreDiff < 0) {
                    return -21;
                }
                return 0;
            }

            int heuristic = 0;
            // Caso no terminal

            // Primero - asignamos 3 puntos por cada esquina tomada (por el blanco) y restamos 5 por cada esquina tomada por negro
            // (total maximo de 8 puntos de esquinas)

            int[] esquinas = new int[4];
            esquinas[0] = tiles[0][0];
            esquinas[1] = tiles[0][size-1];
            esquinas[2] = tiles[size-1][0];
            esquinas[3] = tiles[size-1][size-1];
            for (int i = 0; i < 4; i++) {
                heuristic += 3 * esquinas[i]; // Si la esquina esta tomada por el negro vale -1, asi que restaria 3
            }

            // Segundo - Asignamos puntos si el jugador negro solo tiene 1 movimiento forzado ( 4 puntos maximo por limitacion de movimiento)
            if (vm0.size() == 1) {
                if (vm0.get(0).passed) {
                    heuristic += 4; // Solo puede pasar
                } else {
                    heuristic += 2; // Solo puede hacer un movimiento (sin pasar(
                }
            }

            if (vm1.size() == 1) {
                if (vm1.get(0).passed) {
                    heuristic -= 4; // Solo puede pasar
                } else {
                    heuristic -= 2; // Solo puede hacer un movimiento (sin pasar)
                }
            }

            // Ultimos 4 puntos de heuristica son por los puntajes
            if (scoreDiff > 4) {
                heuristic += 4;
            } else if (scoreDiff > 2) {
                heuristic += 2;
            } else if (scoreDiff < -4) {
                heuristic -= 4;
            } else if (scoreDiff < -2) {
                heuristic -= 2;
            }

            // Si la heuristica queda en 0, la balanceamos un poco para cual jugador tiene mas movimientos
            if (heuristic == 0) {
                if (whiteMoves > blackMoves) {
                    heuristic++;
                } else if ( whiteMoves < blackMoves) {
                    heuristic--;
                }
            }
            return heuristic;
        }

        public int[] scores() {
            int[] scores = new int[2];
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    int tileValue = tiles[i][j];
                    if (tileValue == 1) {
                        scores[1]++;
                    }
                    if (tileValue == -1) {
                        scores[0]++;
                    }
                }

            }
            return scores;
        }
        public boolean gameOver() {

            List<Board> vm0 = getValidMoves(0);
            List<Board> vm1 = getValidMoves(1);
            return vm0.size() == 1 && vm0.get(0).passed && vm1.size() == 1 && vm1.get(0).passed;
        }


        public int[][] cloneTiles(int[][] tiles) {
            int[][] clone = new int[size][size];
            for (int i = 0; i < size; i++) {
                clone[i] = tiles[i].clone();
            }
            return clone;
        }
    }

}
