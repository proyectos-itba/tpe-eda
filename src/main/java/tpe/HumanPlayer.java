package tpe;

import java.io.Serializable;

public class HumanPlayer implements Player, Serializable {
    int playerNumber;

    public HumanPlayer(int playerNumber) {
        this.playerNumber = playerNumber;
    }

    @Override
    public boolean isAi() {
        return false;
    }

    public int[] getMove(Game.Board board) {
        int[] move = new int[2];
        return move;
    }

    @Override
    public int getNumber() {
        return playerNumber;
    }

    @Override
    public String label() {
        return "Human" + (playerNumber == 1? "(White)" : "(Black)");
    }
}

