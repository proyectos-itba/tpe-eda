package tpe;

public interface Player {
//    Devuelve el proximo movimiento como array ( x: arr[0], y: arr[1] )
    public int[] getMove(Game.Board board);

    public int getNumber();

    public boolean isAi();

    public String label();
}
